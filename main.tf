provider "aws" {
  region = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}
variable "region" {}
variable "access_key" {}
variable "secret_key" {}
resource "aws_vpc" "myvpc" {
  cidr_block = "10.0.0.0/24"
  tags = {
    Name = "my_vpc"
  }
}
resource "aws_subnet" "mysubnet" {
  cidr_block = "10.0.0.0/25"
  vpc_id = aws_vpc.myvpc.id
  tags = {
     Name = "my-subnet"
  }
}
resource "aws_instance" "myinstance" {
  count = "3"
  ami = "ami-052efd3df9dad4825"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.mysubnet.id
  tags = {
    Name = "vivek"
  }
}